
//Code to run once body has loaded
//Initialises other functions etc.

const carExample = {

    mf: "ford",
    model: "fiesta",
    colour: "red"


}

function init() {

    console.log({ carExample });
    saveCar();
}

function saveCar() {

    localStorage.setItem("car", JSON.stringify(carExample));

}

function retrieveCar() {

    var carString = localStorage.getItem("car");

    var carObject = JSON.parse(carString);

    console.log({carObject});

    var x = document.getElementById("tbMf");
    x.value = carObject.mf;
    var y = document.getElementById("tbModel");
    y.value = carObject.model;
    var w = document.getElementById("tbColour");
    w.value = carObject.colour

}