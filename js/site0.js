
//Code to run once body has loaded
//Initialises other functions etc.
function init() {

  //  saveFlavour("Vanilla");
   // getFlavour();
}

function saveFlavour() {
    var flavour = document.getElementById("tbFlavour");
    console.log(flavour.value);
    localStorage.setItem('favouriteFlavour', flavour.value);

}

function getFlavour() {

    flavour = localStorage.getItem('favouriteFlavour');

    console.log(`Your favourite flavour is ${flavour}`);

    var divObject = document.getElementById('favFlavour');
    divObject.innerHTML = flavour;
}